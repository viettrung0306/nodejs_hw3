const bcrypt = require('bcrypt');
const {UserModel} = require('../models/User');
const jwt = require('jsonwebtoken');

const registerFunction = async ({email, password, role}) => {
  const salt = await bcrypt.genSalt(10);
  const user = new UserModel({
    role,
    email,
    password: await bcrypt.hash(password, salt),
  });
  await user.save();
};

const signInFunction = async (user) => {
  const email = user.email;
  return jwt.sign({
    user_id: user._id,
    email,
  }, process.env.TOKEN_SECRET, {
    expiresIn: '2h',
  });
};

const forgotPasswordFunction = async (email) => {
  const message = 'New password sent to your email address';
  await sendEmail(email, 'Password reset', message);
};

module.exports = {
  registerFunction,
  signInFunction,
  forgotPasswordFunction,
};
