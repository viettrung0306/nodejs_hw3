const {UserModel} = require('../models/User');

const getUserInfo = async (id) => {
  const user = await UserModel.findOne({_id: id});
  if (!user) {
    throw new Error('User doenst exist');
  }
  return {
    user: {
      _id: user._id,
      role: user.role.toUpperCase(),
      email: user.email,
      created_date: user.created_date,
    },
  };
};

const deleteUserbyUserID = async (id) => {
  await UserModel.findOneAndDelete({_id: id}, (err, docs) => {
    if (err) {
      throw new Error('Cant delete the User');
    }
  });
};

const changeUserPasswordByUserID = async (id, newPassword) => {
  await UserModel.updateOne({_id: id}, {$set: {password: newPassword}});
};
module.exports = {
  getUserInfo,
  deleteUserbyUserID,
  changeUserPasswordByUserID,
};
