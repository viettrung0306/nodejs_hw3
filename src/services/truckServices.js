const TruckModel = require('../models/Truck');
const {UserModel} = require('../models/User');

const getTrucksInfo = async (userId) => {
  const user = await UserModel.findOne({_id: userId});
  const trucksIds = user.trucks;
  if (trucksIds.length === 0) {
    const err = new Error('You dont have cars yet');
    err.status = 400;
    throw err;
  }
  const trucks = await TruckModel.find({_id: {$in: trucksIds}});
  return trucks.map((truck) => {
    return {
      _id: truck._id,
      created_by: truck.created_by,
      assigned_to: truck.assigned_to,
      type: truck.type,
      status: truck.status,
      created_date: truck.created_date,
    };
  });
};

const addNewTruck = async (userId, type) => {
  const truck = new TruckModel({
    type: type.toUpperCase(),
    created_by: userId,
  });
  console.log(truck);
  await truck.save();
  const user = await UserModel.findOneAndUpdate(
    {_id: userId},
    {$push: {trucks: truck._id}},
  );
  await user.save();
};

const getTrucksInfoByTruckID = async (truckId, userId) => {
  await TruckModel.findOne({_id: truckId, userId});
};

const updateTruckInfo = async (truckId, userId, data) => {
  await TruckModel.findOneAndUpdate({_id: truckId, userId}, {$set: data});
};

const deleteTruck = async (truckId, userId) => {
  await TruckModel.findOneAndRemove({_id: truckId, userId});
};

const assignTruck = async ({truckId, userId}) => {
  const user = await UserModel.findOne({_id: userId});
  const currTruck = await TruckModel.findOne({_id: user.assignedTruck});

  if (currTruck && String(currTruck.status).toUpperCase() === 'OL') {
    const err = new Error('U cant reassing truck while ON LOAD');
    err.status = 400;
    throw err;
  }

  const truck = await TruckModel.findOne({_id: truckId});
  user.assignedTruck = truck._id;
  truck.assigned_to = userId;
  await truck.save();
  await user.save();
};

module.exports = {
  getTrucksInfo,
  addNewTruck,
  getTrucksInfoByTruckID,
  updateTruckInfo,
  deleteTruck,
  assignTruck,
};
