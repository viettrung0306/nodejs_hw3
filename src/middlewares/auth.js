const jwt = require('jsonwebtoken');
const {UserModel} = require('../models/User');

const auth = async (req, res, next) => {
  const token = req.header('Authorization').replace('Bearer ', '');
  const data = jwt.verify(token, process.env.TOKEN_SECRET);
  try {
    const user = await UserModel.findOne({
      'email': data.email,
      'tokens.token': token,
    });
    if (!user) {
      throw new Error();
    }
    req.user = user;
    req.token = token;
    next();
  } catch (error) {
    res.status(401).send({error: 'Not authorized to access this resource'});
  }
};

module.exports = auth;
