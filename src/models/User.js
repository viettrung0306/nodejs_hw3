const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    set: function(str) {
      return str.toUpperCase();
    },
    get: function(str) {
      return str.toUpperCase(str);
    },
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  trucks: {
    type: [mongoose.Schema.Types.ObjectId],
    default: [],
    ref: 'trucks',
    get: function(trs) {
      if (this.role === 'DRIVER') {
        return trs;
      }
      return 'Have no access';
    },
  },
  assignedTruck: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
    ref: 'trucks',
    get: function(truck) {
      if (this.role === 'DRIVER') {
        return truck;
      }
      return 'Have no access';
    },
  },
  assignedLoads: {
    type: [mongoose.Schema.Types.ObjectId],
    default: null,
    ref: 'Loads',
    get: function(assignedLoads) {
      const userRole = String(this.role).toUpperCase();
      if (userRole === 'DRIVER') {
        return assignedLoads;
      }
      return 'Have no access to assigned loads';
    },
  },
  loads: {
    type: [mongoose.Schema.Types.ObjectId],
    default: null,
    ref: 'Loads',
    get: function(loads) {
      const userRole = String(this.role).toUpperCase();
      if (userRole === 'SHIPPER') {
        return loads;
      }
      return 'Have no access to loads';
    },
  },
});

const UserModel = mongoose.model('Users', userSchema);

const validate = (user) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  return schema.validate(user);
};

module.exports = {
  UserModel,
  validate,
};
