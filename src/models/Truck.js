const mongoose = require('mongoose');
// const {dimensionsSchema} = require('../models/Load');

const truckDimensions = {
  SPRINTER: {
    height: 300,
    width: 250,
    length: 170,
    payload: 1700,
  },
  SMALL_STRAIGHT: {
    height: 500,
    width: 250,
    length: 170,
    payload: 2500,
  },
  LARGE_STRAIGHT: {
    height: 700,
    width: 350,
    length: 200,
    payload: 4000,
  },
};

const truckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
    required: true,
    set: function(str) {
      return str.toUpperCase();
    },
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL_STRAIGHT', 'LARGE_STRAIGHT'],
    required: true,
    // set: function(type) {
    //   type = type.toUpperCase();
    //   this.parameters = type;
    //   return type;
    // },
  },
  // parameters: {
  //   type: dimensionsSchema,
  //   required: true,
  //   set: function() {
  //     return ({width, height, length} = truckDimensions[type]);
  //   },
  //   default: function() {
  //     return ({width, height, length} = truckDimensions[this.type]);
  //   },
  // },
  payload: {
    type: Number,
    required: true,
    default: function() {
      return truckDimensions[this.type].payload;
    },
  },
});

module.exports = mongoose.model('Trucks', truckSchema);
