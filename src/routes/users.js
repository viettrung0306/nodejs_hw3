const express = require('express');
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/userController');
const auth = require('../middlewares/auth');
const router = new express.Router();

router.get('/', auth, getUser);

router.delete('/', auth, deleteUser);

router.patch('/password', auth, changeUserPassword);

module.exports = router;
