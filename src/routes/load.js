const express = require('express');
const auth = require('../middlewares/auth');
const {
  getLoads,
  addLoads,
  getActiveLoads,
  patchLoadState,
  getLoadsByID,
  updateLoadsByID,
  deleteLoadsByID,
  postLoadByID,
  getLoadShippingInfoByID,
} = require('../controllers/loadController');
const router = new express.Router();

router.get('/', auth, getLoads);

router.post('/', auth, addLoads);

router.get('/active', auth, getActiveLoads);

router.patch('/active/state', auth, patchLoadState);

router.get('/:id', auth, getLoadsByID);

router.put('/:id', auth, updateLoadsByID);

router.delete('/:id', auth, deleteLoadsByID);

router.post('/:id/post', auth, postLoadByID);

router.get('/:id/shipping_info', auth, getLoadShippingInfoByID);

module.exports = router;
