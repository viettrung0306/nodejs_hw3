const express = require('express');
const auth = require('../middlewares/auth');
const {
  getTrucks,
  addTrucks,
  getTrucksByTruckID,
  updateTruckByID,
  deleteTruckByID,
  assignTruckToUser,
} = require('../controllers/truckController');
const router = new express.Router();

router.get('/', auth, getTrucks);

router.post('/', auth, addTrucks);

router.get('/:id', auth, getTrucksByTruckID);

router.put('/:id', auth, updateTruckByID);

router.delete('/:id', auth, deleteTruckByID);

router.post('/:id/assign', auth, assignTruckToUser);

module.exports = router;
