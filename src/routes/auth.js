/* eslint-disable linebreak-style */
const express = require('express');
const router = new express.Router();
const {
  registration,
  signIn,
  forgotPassword,
} = require('../controllers/authController');

router.post('/register', registration);

router.post('/login', signIn);

router.post('/forgot_password', forgotPassword);

module.exports = router;
