const bcrypt = require('bcrypt');
const Joi = require('joi');
const {UserModel} = require('../models/User');
const {
  registerFunction,
  signInFunction,
} = require('../services/authServices');

const registration = async (req, res) => {
  const body = req.body;
  if (!(body.email && body.password)) {
    return res.status(400).send({message: 'string'});
  }

  await UserModel.findOne({email: body.email}).then((user) => {
    if (user) {
      res.status(400).send({message: 'String'});
    }
  });

  await registerFunction(body).then((doc) => res.status(200).send(doc));
};

const signIn = async (req, res) => {
  const body = req.body;
  if (!(body.email && body.password)) {
    return res.status(400).send({message: 'not Body'});
  }
  const user = await UserModel.findOne({email: body.email});

  if (!user) {
    res.status(400).send({message: 'not user string'});
    return;
  }

  const token = await signInFunction(user);
  const validatePassword = await bcrypt.compare(body.password, user.password);

  if (validatePassword) {
    res.status(200).send({message: 'Success', user, accessToken: token});
  } else {
    res.status(400).send({message: 'string'});
  }
};

const forgotPassword = async (req, res) => {
  try {
    const schema = Joi.object({email: Joi.string().email().required()});
    const {error} = schema.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const user = await UserModel.findOne({email: req.body.email});

    if (!user) {
      return res.status(400).send({message: 'string'});
    }
    await forgotPasswordFunction(user.email);
    res
      .status(200)
      .send({message: 'New password sent to your email address'});
  } catch (error) {
    res.status(400).send({message: 'string'});
  }
};

module.exports = {
  registration,
  signIn,
  forgotPassword,
};
