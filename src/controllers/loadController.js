const {
  getUserLoads,
  createLoad,
  getUserActiveLoads,
  patchLoadByState,
  getUserLoadsByID,
  updateUserLoadById,
  deleteUserLoadById,
} = require('../services/loadServices');

const getLoads = async (req, res) => {
  const userID = req.user._id;
  const {status, limit = 10, offset = 0} = req.query;
  const loads = await getUserLoads({userID, status, limit, offset});
  res.status(200).json({
    message: 'Success',
    loads: loads,
  });
};

const addLoads = async (req, res) => {
  const {_id: created_by} = req.user;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    width,
    length,
    height,
  } = req.body;
  await createLoad({
    created_by,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    width,
    length,
    height,
  });
  res.status(200).json({message: 'Load created successfully'});
};

const getActiveLoads = async (req, res) => {
  const {_id, role} = req.user;
  const load = await getUserActiveLoads({_id, role});
  res.status(200).send({message: 'Success', loads: load});
};

const patchLoadState = async (req, res) => {
  const {_id, role} = req.user;
  const load = await patchLoadByState({_id, role});
  res.status(200).json({message: `Load state changed to ${load.state}`});
};

const getLoadsByID = async (req, res) => {
  const {_id: userId, role} = req.user;
  const {id: loadId} = req.params;
  const load = await getUserLoadsByID({userId, role, loadId});
  res.status(200).send({load: load});
};

const updateLoadsByID = async (req, res) => {
  const loadSetting = req.body;
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;

  await updateUserLoadById(loadSetting, {loadId, userId, role});
  res.status(200).json({message: 'Load details changed successfully'});
};

const deleteLoadsByID = async (req, res) => {
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;
  await deleteUserLoadById({loadId, userId, role});
  res.status(200).json({message: 'Load was deleted'});
};

const postLoadByID = async (req, res) => {
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;

  if (role !== 'SHIPPER') return res.status(400).send({message: 'string'});

  const load = await postUserLoadById({loadId, userId, role});
  const truck = await searchDriver(load);

  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: !!truck,
  });
};

const getLoadShippingInfoByID = async (req, res) => {
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;
  const {load, truck} = await getUserLoadShippingInfoById({
    loadId,
    userId,
    role,
  });
  res.status(200).json({load: load, truck: truck});
};

module.exports = {
  getLoads,
  addLoads,
  getActiveLoads,
  patchLoadState,
  getLoadsByID,
  updateLoadsByID,
  deleteLoadsByID,
  postLoadByID,
  getLoadShippingInfoByID,
};
