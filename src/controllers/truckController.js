const {
  getTrucksInfo,
  addNewTruck,
  getTrucksInfoByTruckID,
  updateTruckInfo,
  deleteTruck,
  assignTruck,
} = require('../services/truckServices');

const getTrucks = async (req, res) => {
  const userId = req.user._id;
  if (req.user.role !== 'DRIVER') {
    return res.status(400).json({message: 'You Have No Access To This'});
  }
  const trucks = await getTrucksInfo(userId);
  console.log(trucks);
  res.status(200).json({message: 'success', trucks: [...trucks]});
};

const addTrucks = async (req, res) => {
  const userId = req.user._id;
  const type = req.body.type;

  console.log(userId, type);
  if (!(userId && type)) {
    return res.status(400).send({message: 'string'});
  }
  await addNewTruck(userId, type);
  res.status(200).send({message: 'Truck has been added'});
};

const getTrucksByTruckID = async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  const truck = await getTrucksInfoByTruckID(truckId, userId);
  if (!truck) res.status(400).send({message: 'string'});

  res.status(200).send({truck: truck});
};

const updateTruckByID = async (req, res) => {
  const truckId = req.params.id;
  const userId = req.user._id;
  const data = req.body;
  const truck = await getTrucksInfoByTruckID(truckId, userId);

  if (!truck) return res.status(400).send({message: 'String'});

  await updateTruckInfo(truckId, userId, data);
  res.status(200).send({message: 'Success'});
};

const deleteTruckByID = async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;

  const truck = await getTrucksInfoByTruckID(truckId, userId);
  if (!truck) return res.status(400).send({message: 'String'});
  await deleteTruck(truckId, userId);
  res.status(200).send({message: 'Success'});
};

const assignTruckToUser = async (req, res) => {
  const truckId = req.params.id;
  const userId = req.user._id;
  await assignTruck({truckId, userId});
  res.status(200).json({message: 'The truck has been assigned to you'});
};

module.exports = {
  getTrucks,
  addTrucks,
  getTrucksByTruckID,
  updateTruckByID,
  deleteTruckByID,
  assignTruckToUser,
};
