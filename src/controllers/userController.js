const {
  getUserInfo,
  deleteUserbyUserID,
  changeUserPasswordByUserID,
} = require('../services/userServices');
const bcrypt = require('bcrypt');

const getUser = async (req, res) => {
  try {
    const userId = req.user._id;
    const userInfo = await getUserInfo(userId);
    res.status(200).json(userInfo);
  } catch (error) {
    res.status(400).json(error);
  }
};

const deleteUser = async (req, res) => {
  try {
    const userId = req.user._id;
    await deleteUserbyUserID(userId);
    res.status(200).send({message: 'Profile deleted successfully'});
  } catch (err) {
    res.status(400).json({Error: error});
  }
};

const changeUserPassword = async (req, res) => {
  const user = req.user;
  const {oldPassword, newPassword} = req.body;
  if (user.password !== oldPassword) {
    res.status(400).send({message: 'String'});
  }
  const newHashedPass = await bcrypt.hash(newPassword, 10);
  await changeUserPasswordByUserID(user._id, newHashedPass);
  res.status(200).send({message: 'Success'});
};

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
